import StartupHead from "../components/startup/StartupHead";

const Startup = () => {
    return(
        <div>
            <StartupHead/>
        </div>
    );
}

export default Startup;