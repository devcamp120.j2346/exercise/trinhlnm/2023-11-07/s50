import { Box, Container, Grid } from "@mui/material";
import "../App.css";
import LoginContent from "../components/LoginContent";

const LoginPage = () => {
    return (
        <Container maxWidth className="bg1" style={{ padding: "0px" }}>
            <Grid container>
                <Grid item md={4} className="left-content">
                    <Box style={{ width: "80%" }}>
                        <LoginContent />
                    </Box>
                </Grid>
            </Grid>
        </Container>
    );
}

export default LoginPage;