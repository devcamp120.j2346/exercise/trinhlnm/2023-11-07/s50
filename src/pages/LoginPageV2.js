import { Box, Container, Grid } from "@mui/material";
import "../App.css";
import LoginV2Content from "../components/LoginV2Content";

const LoginPageV2 = () => {
    return(
        <Container maxWidth className="bg" style={{padding: "0px"}}>
            <Grid container>
                <Grid item md={6} className="left-content">
                    <Box style={{width: "60%"}}>
                        <LoginV2Content/>
                    </Box>
                </Grid>
            </Grid>
        </Container>
    );
}

export default LoginPageV2;