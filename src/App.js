import LoginPage from "./pages/LoginPage";
import LoginPageV2 from "./pages/LoginPageV2";
import Startup from "./pages/Startup";
import TravelBlog from "./pages/TravelBlog";

function App() {
  return (
    <div>
      {/* <LoginPage/> */}

      {/* <LoginPageV2/> */}

      {/* <TravelBlog/> */}

      <Startup/>
    </div>
  );
}

export default App;
