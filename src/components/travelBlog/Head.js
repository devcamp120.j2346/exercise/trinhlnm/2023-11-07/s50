import { AppBar, Box, Container, Grid, Icon, Toolbar } from "@mui/material";
import "bootstrap/dist/css/bootstrap.min.css";
import "./style.css"
import { Button } from "bootstrap";
import { Height } from "@mui/icons-material";

const Head = () => {
    return (
        <Box>
            <AppBar position="static" elevation={0} sx={{ backgroundColor: "transparent", padding: "0rem 3rem" }}>
                <Toolbar sx={{ justifyContent: "space-between" }}>
                    <img src={require("../../assets/images/travelBlog/HeaderLogo.png")} style={{ width: "6rem" }} />
                    <div style={{ display: "flex" }}>
                        <div style={{ borderRight: "1px solid #EEE" }}>
                            <span className="menu-item" style={{ color: "#8A53FF" }}>Home</span>
                            <span className="menu-item">Story</span>
                            <span className="menu-item">Gallery</span>
                            <span className="menu-item">Contact Us</span>
                        </div>
                        <div>
                            <img src={require("../../assets/images/travelBlog/HeaderSearchIcon.png")} style={{ width: "20px", margin: "auto 1rem" }} />
                            <img src={require("../../assets/images/travelBlog/HeaderProfile.png")} style={{ width: "20px" }} />
                        </div>
                    </div>
                </Toolbar>
            </AppBar>

            <Container maxWidth className="bg">
                <Grid container>
                    <Grid item md={6} className="travel-left-content">
                        <div>
                            <div className="travel-let">
                                <span style={{ color: "#8a53ff" }}>Let’s talk</span> about your next trip!
                            </div>
                            <div className="travel-share">Share your favorite travel destination and we will feature it in our next blog!</div>
                            <button className="travel-btn-share">Share your story</button>
                        </div>
                    </Grid>
                </Grid>
            </Container>
        </Box>
    );
}

export default Head;