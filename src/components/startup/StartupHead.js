import { AppBar, Box, Grid, IconButton, Menu, MenuItem, Toolbar } from "@mui/material";
import MenuIcon from '@mui/icons-material/Menu';
import "./style.css";
import "bootstrap/dist/css/bootstrap.min.css";
import { useState } from "react";

const StartupHead = () => {
    const [anchorEl, setAnchorEl] = useState(null);
    const open = Boolean(anchorEl);
    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };
    const handleClose = () => {
        setAnchorEl(null);
    };

    return (
        <Box sx={{ backgroundColor: "#74C69D", minHeight: "100vh" }}>
            <AppBar position="relative" elevation={0} sx={{ backgroundColor: "transparent", padding: "1rem 3rem" }}>
                <Toolbar sx={{ justifyContent: "space-between" }}>
                    <img src={require("../../assets/images/startup/Frame4.png")} />

                    <div style={{ display: "block" }} className="d-md-none">
                        <IconButton
                            size="large"
                            edge="start"
                            color="inherit"
                            aria-label="menu"
                            sx={{ mr: 2, border: "1px solid white", padding: "5px", margin: "0px" }}
                            id="basic-button"
                            aria-controls="basic-menu"
                            aria-haspopup="true"
                            aria-expanded={open ? 'true' : undefined}
                            onClick={handleClick}
                        >
                            <MenuIcon />
                        </IconButton>

                        <Menu
                            id="basic-menu"
                            anchorEl={anchorEl}
                            open={open}
                            onClose={handleClose}
                            MenuListProps={{
                                'aria-labelledby': 'basic-button',
                            }}
                        >
                            <MenuItem onClick={handleClose}>Home</MenuItem>
                            <MenuItem onClick={handleClose}>Portfolio</MenuItem>
                            <MenuItem onClick={handleClose}>Services</MenuItem>
                            <MenuItem onClick={handleClose}>Contact</MenuItem>
                        </Menu>
                    </div>

                    <div style={{ display: "none" }} className="d-md-block">
                        <span className="startup-menu-item">Home</span>
                        <span className="startup-menu-item">Portfolio</span>
                        <span className="startup-menu-item">Services</span>
                        <span className="startup-menu-item" style={{ marginRight: "0rem" }}>Contact</span>
                    </div>
                </Toolbar>
            </AppBar>

            <Grid container direction={{md: "row", xs: "column-reverse"}}>
                <Grid item md={5} sx={{ padding: "0rem 3rem" }}>
                    <div className="st-wel">WELCOME</div>
                    <div className="st-lo1">Lorem ipsum dolor sit amet consectetur </div>
                    <div className="st-lo2">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Suscipit nemo hic quos, ab, dolor aperiam nobis cum est eos error ipsum, voluptate culpa nesciunt delectus iste?</div>
                    <button className="st-btn-ex">Explore</button>
                </Grid>
                <Grid className="startup-img-container" item md={7} sx={{ display: "flex", alignItems: "center" }}>
                    <img src={require("../../assets/images/startup/Group.png")} style={{ width: "90%" }} />
                </Grid>
            </Grid>
        </Box >
    );
}

export default StartupHead;