import { PersonOutlined, VisibilityOffOutlined, VisibilityOutlined } from "@mui/icons-material";
import { Box, Button, FilledInput, FormControl, IconButton, InputAdornment, InputLabel, Typography } from "@mui/material";
import React from "react";
import "../App.css"

const LoginV2Content = () => {
    const [showPassword, setShowPassword] = React.useState(false);

    const handleClickShowPassword = () => setShowPassword((show) => !show);

    const handleMouseDownPassword = (event) => {
        event.preventDefault();
    };

    return (
        <Box sx={{ textAlign: "center" }}>
            <Box className="tittle">
                Đăng nhập
            </Box>

            <FormControl sx={{ width: '100%', borderBottom: "1px solid rgba(0, 0, 0, 0.44)", marginBottom: "2rem" }} variant="filled">
                <InputLabel className="log-input">Tên tài khoản</InputLabel>
                <FilledInput
                    id="filled-adornment-password"
                    endAdornment={
                        <InputAdornment position="end">
                            <IconButton edge="end">
                                <PersonOutlined />
                            </IconButton>
                        </InputAdornment>
                    }
                />
            </FormControl>

            <FormControl sx={{ width: '100%', borderBottom: "1px solid rgba(0, 0, 0, 0.44)", marginBottom: "2rem" }} variant="filled">
                <InputLabel htmlFor="filled-adornment-password" className="log-input">Mật khẩu</InputLabel>
                <FilledInput
                    id="filled-adornment-password"
                    type={showPassword ? 'text' : 'password'}
                    endAdornment={
                        <InputAdornment position="end">
                            <IconButton
                                aria-label="toggle password visibility"
                                onClick={handleClickShowPassword}
                                onMouseDown={handleMouseDownPassword}
                                edge="end"
                            >
                                {showPassword ? <VisibilityOutlined /> : <VisibilityOffOutlined />}
                            </IconButton>
                        </InputAdornment>
                    }
                />
            </FormControl>

            <Button variant="contained" style={{ width: "100%" }} className="log-btn">ĐĂNG NHẬP</Button>
        </Box>

    );
}

export default LoginV2Content;