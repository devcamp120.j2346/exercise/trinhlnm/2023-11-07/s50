import { PersonOutlined, VisibilityOffOutlined, VisibilityOutlined } from "@mui/icons-material";
import { Box, Button, FilledInput, FormControl, IconButton, Input, InputAdornment, InputLabel, Typography } from "@mui/material";
import React from "react";
import "../App.css"

const LoginContent = () => {
    const [values, setValues] = React.useState({
        amount: '',
        password: '',
        weight: '',
        weightRange: '',
        showPassword: false,
    });

    const handleChange = (prop) => (event) => {
        setValues({ ...values, [prop]: event.target.value });
    };

    const handleClickShowPassword = () => {
        setValues({
            ...values,
            showPassword: !values.showPassword,
        });
    };

    const handleMouseDownPassword = (event) => {
        event.preventDefault();
    };

    return (
        <Box sx={{ textAlign: "center" }}>
            <Box className="tittle1">
                Login
            </Box>

            <FormControl sx={{ width: '100%', borderBottom: "1px solid white", marginBottom: "2rem" }} variant="standard">
                <InputLabel className="log-input1">Username</InputLabel>
                <Input
                    sx={{ color: "white" }}
                    id="filled-adornment-password"
                    endAdornment={
                        <InputAdornment position="end">
                            <IconButton>
                                <PersonOutlined sx={{ color: "white" }} />
                            </IconButton>
                        </InputAdornment>
                    }
                />
            </FormControl>

            <FormControl sx={{ width: '100%', borderBottom: "1px solid white", marginBottom: "2rem" }} variant="standard">
                <InputLabel htmlFor="standard-adornment-password" className="log-input1">Password</InputLabel>
                <Input
                    sx={{ color: "white" }}
                    id="standard-adornment-password"
                    type={values.showPassword ? 'text' : 'password'}
                    value={values.password}
                    onChange={handleChange('password')}
                    endAdornment={
                        <InputAdornment position="end">
                            <IconButton
                                aria-label="toggle password visibility"
                                onClick={handleClickShowPassword}
                                onMouseDown={handleMouseDownPassword}
                            >
                                {values.showPassword ? <VisibilityOutlined sx={{ color: "white" }} /> : <VisibilityOffOutlined sx={{ color: "white" }} />}
                            </IconButton>
                        </InputAdornment>
                    }
                />
            </FormControl>

            <Button variant="contained" style={{ width: "100%" }} className="log-btn1">Login</Button>
        </Box>

    );
}

export default LoginContent;